<?php
/***** Equipment Fine Processing Email Content ***********************
 * Purpose: Contains the formatted text for the body of the emails sent by the equipFines.php script 
 * Created: 2019-01-10 by M. Gauthier for MRU Library
 ********************************************************************************/

define('EMAIL_DIV_HTML', '<div style="font-family:arial;color:#333;margin:0;padding:0em;font-size:14px;">');define('LIBRARY_NAME', 'Library Name');
define('LIBRARY_ADDRESS1', 'Building Name');
define('LIBRARY_ADDRESS2', 'Street Address');
define('LIBRARY_ADDRESS3', 'City');
define('LIBRARY_ADDRESS4', 'Postal Code');
define('LIBRARY_PHONE', '555.123.4567');
define('LIBRARY_SERVICE_DESK', 'Library Service Desk');



function getEmailHeading($subject, $name, $date){
	$const = get_defined_constants();
	
	$heading = <<<EOD
<table cellspacing="0" cellpadding="5" border="0" style="background-color:#e9e9e9;width:100%;height:30px">
	<tbody>
		<tr style="background-color:#ffffff;width:100%">
			<td colspan="2">
			</td>
		</tr>
		<tr>
			<td><h1>$subject</h1></td>
			<td align="right">$date</td>
		</tr>
	</tbody>
</table>
<table cellspacing="0" cellpadding="5" border="0" width="100%">
	<tbody>
		<tr>
			<td width="50%">
				<table cellspacing="0" cellpadding="5" border="0" style="list-style=:none;margin:0 0 0 1em;padding:0">
					<tbody>
						<tr>
							<td></td>
						</tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
						<tr><td></td></tr>
					</tbody>
				</table>
			</td>
			<td width="50%" align="right">
				<table style="list-style:none;margin:0 0 0 1em;padding:0">
					<tbody>
						<tr>
							<td>{$const['LIBRARY_NAME']}</td>
						</tr>
						<tr>
							<td>{$const['LIBRARY_ADDRESS1']}</td>
						</tr>
						<tr>
							<td>{$const['LIBRARY_ADDRESS2']}</td>
						</tr>
						<tr>
							<td>{$const['LIBRARY_ADDRESS3']}</td>
						</tr>
						<tr>
							<td>{$const['LIBRARY_ADDRESS4']}</td>
						</tr>
						<tr>
							<td></td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>	
EOD;
	return $heading;
}

function getEmailClosing(){
	$const = get_defined_constants();
	$closing = <<<EOD
	<p>Please contact the {$const['LIBRARY_SERVICE DESK']} at {$const['LIBRARY_PHONE']} if you have any questions.</p>	
	<p>Sincerely,<br>{$const['LIBRARY_SERVICE DESK']}</p>
EOD;
	return $closing;
}

/*****************************************************************************
 * Compose the string for the email body message
 * @param string $itemName lost item name
 * @param string $loanDate lost item loan date
 * @param string $dueDate lost item due date
 * @param string $subject email subject line
 * @param &$feesArray reference to a double-scripted array with 
 *        the fee description in element 0 and the fee amount in element 1
 * @return string $bodyContent the formatted message body string for the email
 *****************************************************************************/
function generateLostEmailBody($patronName, $itemName, $dueDate, $subject, &$feesArray){
	
	$header = getEmailHeading($subject, $patronName, date("m/d/Y"));
	$closing = getEmailClosing();
	$bodyContent = <<<EOD
	<html>
	<body>
EOD;
	$bodyContent .= EMAIL_DIV_HTML . $header;
	
	$bodyContent .= <<<EOD
	<p>Dear $patronName:</p>
	<p>This is to inform you that the equipment with the details below, borrowed by you, has been declared as lost.</p>
	<ul>
		<li>Item name: $itemName</li>
		<li>Due date: $dueDate </li>
	</ul>
	<p>You will be charged the following fines and fees:</p>
	<ul>
EOD;
	foreach($feesArray as $fee){
		$feeDesc = $fee[0];
		$amount = sprintf('%01.2f',$fee[1]);
		$bodyContent .= "<li>$feeDesc: $amount </li>";
	}
	$bodyContent .= <<<EOD
	</ul>
	<p>If you return the equipment to the Library, the <strong>Equipment lost item replacement fee</strong> will be refunded. 
     <strong>The Equipment overdue fine</strong> and the <strong>Equipment lost item processing fee</strong> will remain.</p>
	$closing
	</div>
	</body>
	</html>
EOD;
	return $bodyContent;
}

/*****************************************************************************
 * Compose the string for the email body message
 * @param string $itemName lost item name
 * @param string $loanDate lost item loan date
 * @param string $dueDate lost item due date
 * @param string $lostDate date on which item will be declared lost
 * @param string $subject email subject line
 * @param &$feesArray reference to a double-scripted array with 
 *        the fee description in element 0 and the fee amount in element 1
 * @return string $bodyContent the formatted message body string for the email
 *****************************************************************************/

function generateWarningEmailBody($patronName, $itemName, $dueDate, $lostDate, $subject, &$feesArray){
	
	$header = getEmailHeading($subject, $patronName, date("m/d/Y"));
	$closing = getEmailClosing();
	$bodyContent = <<<EOD
	<html>
	<body>
EOD;
	$bodyContent .= EMAIL_DIV_HTML . $header;
	
	$bodyContent .= <<<EOD
	<p>Dear $patronName:</p>
	<p>This is to inform you that the equipment with the details below, borrowed by you, is long overdue and must be returned. If the equipment is not returned by $lostDate, it will be declared as lost and you will be responsible for replacement and processing fees in addition to overdue fines.</p>
	<ul>
		<li>Item name: $itemName</li>
		<li>Due date: $dueDate </li>
	</ul>
	<p>You will be charged the following fines and fees:</p>
	<ul>
EOD;
	foreach($feesArray as $fee){
		$feeDesc = $fee[0];
		$amount = sprintf('%01.2f',$fee[1]);
		$bodyContent .= "<li>$feeDesc: $amount </li>";
	}
	
	$bodyContent .= <<<EOD
	</ul>
	$closing
	</div>
	</body>
	</html>
EOD;
	return $bodyContent;
}
?>	
