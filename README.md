| Resource Name | Description |
| ------ | ------ |
| equipFinesConfig.php | Defines constants and variables used to customize and configure the process. Update this file with values relevant for your institution.  |
| equipFines.php | Main script  |
| equipEmail.php | Contains the formatted text for two different  lost  item notices: The warning notice that gets sent 3 days before an item is assumed lost. The notice that gets sent once the item is assumed lost and the max overdue fine, replacement value and processing fee are posted to Alma. |
| libcalAPI_Utils.php |	Library of utility routines shared with other LibCal API integration processes |
| sendEmail.php	| Library of smtp mail routines. Requires PHP PEAR module. |

<h1>Configuration  notes:</h1>
<h2>ALMA Set Up</h2><br/>

1. <b>Fine/Fee type:</b> <br/><br/>Decide what Alma fine/fee types you want posted to Alma. If you don’t want to use existing types, contact Ex Libris Customer Support to have them enable custom types. Once the custom types are set up, you can view them in the Fine Fee Type mapping table: <br/><b><i>ALMA > Configuration > User Management > Patron Charges > Fine/Fees Behavior</b></i><br/><br/>The fine/fee codes are used when the fines are posted Alma. The fine/fee names are used in the email notices for patrons. Constant arrays for fine/fee types are defined in the equipFinesConfig.php file. Update these arrays to use the relevant values for your set-up .<br/><br/>NOTE: the Alma interface displays the fine/fee type name in the “Fine/Fee type” field of the Alma table but you can look at the source code for the browser page to determine the fine/fee type code. 

1. <b>Alma API Key</b><br/><br/>On the Ex Libris Developer Network, set up an API key in Alma with “Users Read/Write” permissions. Update the api key variable configured in the equipFinesConfig.php file.

<h2>LibCal Setup</h2>
Set up an API Application in LibCal. In LibCal, go to <b><i>Admin > API < API Authentication > Create New Application</b></i>.


The resulting Client Id number and the Client Secret should then be configured in the $creds array in the equipFinesConfig.php file.<br/>


<b>NOTE:</b> The LibCal API does not currently provide methods for posting fine payments or altering fine balances to LibCal. Because of this, the script does not clear the fines in LibCal. To compensate for this, a member of our Library staff reviews the reports output by this script each week and manually clears the fines from LibCal. 

<h2>PHP Script Custom Variable Configuration:</h2>
The equipFinesConfig.php file contains most of the variables that should be customized with the relevant values for your institution and implementation. As well as the API keys and credentials for your institution, you can define the LibCal locations, Alma fine and waive codes, error log file, email report sender/recipients, default fine amounts, maximum fine amounts, and time-period variables that determine when notices are sent and when overdue equipment items are assumed lost.


<h2>SMTP Email notices:</h2>
If using the included sendEmail.php script to send notices for long-overdue-assumed-lost items, you will need to:<br/>

1. Ensure the PHP PEAR module is installed
1. Configure the $host and $port variables in the sendEmail.php script with the relevant values for your institution.
1. Update the text of the notices in the equipEmail.php script with text suitable for your institution. Consonants have been defined at the top of this file so you can use the existing text, if desired, while easily updating your institutional details.


<h2>PHP Main Script</h2>

The main code is contained in the equipFines.php file. At the top of this file, there are a few variables containing file paths that need to be customized for your implementation. 

*** <b>Add Institution-Specific Patron Lookup Code:</b>***<br/>
An object class named **Fine** is defined in this file. The patron lookup code in the body of the **Fine->setUserInfo()** method has been omitted and needs to be defined for your institution. This method needs to confirm that the patron from LibCal has a valid account in Alma and then populate the patron's name and ALMA identifier in the Fine object. Whether this method is needed and how this is accomplished depends on what identifiers your institution uses in both LibCal and Alma and what tools you can use to validate and retrieve the relevant patron information for Alma. In our case, we use the patron email address available in the LibCal equipment booking to make a call to our Student Information System to get the name and the identifier that is used in Alma.

<h2>Error Reports</h2>

The script emails an error report to the designated recipients defined in the equipFinesConfig.php file. A member of our Library staff reviews the error report. If there is an error posting or waiving a fine/fee in Alma, then staff will manually confirm the details in LibCal and manually post or waive the fines or fees in Alma, as necessary.

The error report also provides warnings if replacement values for items are missing in LibCal. If no replacement value exists for a lost item, a default value is used when the fee is posted. The warning in the error report allows staff to update the LibCal item record with the missing value for future fine/fee postings.

<h1>Overview of Main Script Processes</h1>

<h2>PROCESS OUTSTANDING FINES</h2>

- Fines are assessed in LibCal when an item is checked back in.
- The script uses the LibCal API to retrieve all outstanding fines assessed in LibCal on the previous day for specified equipment locations.
- For each fine assessed on the previous day, it extracts the email address for the patron that was used to book the item and searches our Student Information System for the user.
- If the user is not found, it adds the fine information to an error report.

- If it finds the user, it retrieves the patron name and the identifier we use in Alma for the user.

- - It retrieves the details of the item booking and patron from LibCal.
- - If the fine is for an item that is more than 28 days overdue, it does not assess a fine. It assumes the item has already been processed as lost. 


- - - It makes an API call to Alma to see if an Equipment lost item replacement fee has been charged to the user account. It checks to see if 

- - - - the fee code is “Equipment lost item replacement fee”

- - - - the fee amount is equal to the item replacement value, and 
- - - - there is an item ID in the fee comment field that matches the LibCal item ID. 
- - - If it finds a matching replacement fee, it uses the Alma API to 
- - - - waive the fee with the LOSTITEMWASFOUND waive reason code 
- - - - it adds a comment to the waive transaction: <br/>“Item checked into LibCal equipment on <DATE>
- - - If the fee is successfully waived, it adds it to the “Replacement Fees Waived” section of the processing report.
- - - If there is an error waiving the fee, it adds it to the “Errors” section of the processing report.
 
- - If the fine is less than 28 days overdue
- - - It posts the fine amount from LibCal to the user’s account in Alma using the  Equipment overdue fine type.
- - - - It adds a comment to the fine that includes:
- - - - - the item name
- - - - - the LibCal item ID number
- - - - - The LibCal barcode (which, for us, is the item Call Number)
- - - - - The  item due date
- - - If the fine is posted successfully, it reports it in the “Overdue Fines Posted” section of the processing report.
- - - If the fine cannot be posted to Alma, it reports it in the “Errors” section of the processing report

<h2>PROCESS LOST ITEMS</h2>

- The script uses the LibCal API to retrieve a list of all equipment items for the specified locations that are overdue.
- - If an item is overdue for exactly **25** days - - It sends an email to the patron warning them that they will be charged the max overdue fine, replacement fee, and processing fee if the item is not returned by the end of the 28th day.
- - If an item is overdue for exactly 28 days
- - - It extracts the email address for the patron that was used to book the item and searches our Student Information System for the user. If the user is not found, it adds the fine information to an error report.
- - - If it finds the user, it retrieves the user identifier and name.
- - - It uses the LibCal API to retrieve the details of the item booking.
- - - It uses the Alma API to post the following fines and fees to the user’s account:
- - - - An Equipment overdue fine for the maximum fine amount. (An array in the config file defines the max fine for specific categories.)  If the category ID for the item is defined in the array, the max fine defined for that category is used. If the category ID is not in the array, the default max fine amount defined in the config file is used.
- - - - An Equipment lost item processing fee in the amount of $15.
- - - - An Equipment lost item replacement fee equal to the amount configured for the item value in LibCal. If no ‘value’ is defined for the item in LibCal, then a default is used based on the category ID.
- - - Each fine or fee is posted individually via an API call. If the fine or fee is posted successfully, it is reported in the “Lost Items Posted” section of the processing report.
- - - - An email is sent to the user using the email address that was used for the booking.
- - - - - If there is an error sending the email, it will be reported in the “Errors” section of the processing report..

- - - If there is an error posting the individual fine or fee, an error is displayed in the “Errors” section of the processing report. 


<H2>REPORTING</h2>
A report is emailed to designated recipients detailing:<br/>

1. Equipment overdue fines posted to Alma

2. Equipment lost item fines and fees posted to Alma

3. Waived equipment replacement fees posted to Alma

4. Errors: 
   - Errors accessing the Alma or LibCal APIs 
   - Errors posting fines and fees to Alma






