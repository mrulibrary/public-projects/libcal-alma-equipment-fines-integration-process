<?php
/***** Equipment Fine Processing Settings and Credentials ***********************
 * Purpose: Defines constants and parameters for use in the main equipment fines scripts equipFines.php 
 * Created: 2019-09-26 by M. Gauthier for MRU Library
 ********************************************************************************/

date_default_timezone_set('America/Edmonton');
setlocale(LC_MONETARY,"en_CA");
 
// API parameters for LibCal Equipment Booking Fines application
$creds = array(
	"url" => "https://api2-ca.libcal.com/1.1/",
	"client_id" => "XXX", /* define libcal client ID */
	"client_secret" => "abcdefghijklmnop", /* define libcal api application secret */
	"grant_type" => "client_credentials"
	);

// Alma Production API Key
$almaKey = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"; // define ALMA API Key


// Alma base url for API (used for both Alma Production and Sandbox API)
$almaUsersAPIUrl = "https://api-ca.hosted.exlibrisgroup.com/almaws/v1/users/";

// libcal locations IDs for which fines are processed: if not in this array, location fines won't be processed
define('LIBCAL_LOCATIONS', array("1234", "5678", "9012", "3456")); // location IDs for fines that get posted to Alma

// Hard-coded fine/fee  amounts
define('PROCESS_FEE_AMOUNT', '15'); // amount charged as processing fee for lost items
define('DEFAULT_REPLACEMENT_VALUE', '50');  // default replacement value if none defined for item in LibCal

// set up assoc arrays for differential Max Fine and Replacement Value amounts
define('DEFAULT_FEES_ARRAY', array("MaxFine" => '50', "ReplacementValue" => DEFAULT_REPLACEMENT_VALUE));
define('LAPTOP_FEES_ARRAY', array("MaxFine" => '180', "ReplacementValue" => DEFAULT_REPLACEMENT_VALUE));

// Assign differential max fines and replacement values for LibCal categories. If category is not in the CATEGORY_FEES array, default will be used. 
define('CATEGORY_FEES', array("1234" => LAPTOP_FEES_ARRAY, "5678" => LAPTOP_FEES_ARRAY)); // categories and associated default fine codes/replacement values  

define('EQUIP_MAX_TOKEN_AGE', 3591);  // time in seconds used to trigger new authentication token retrieval (token expires after 3600 seconds)

// File paths
define('DATASTORE_PATH', LIB_FILE_PATH . 'datastore/equipment/'); // file path to directory where output is stored
define('EQUIP_TOKEN_PHP_FILE', DATASTORE_PATH . 'equipFinesAuthToken.php'); // file where authentication token gets stored as a php variable
define('EQUIP_ERROR_LOG_FILE', DATASTORE_PATH . "fines_script_error.log"); // error log file for equip Fines script

// Define Alma fine table codes used
define('OVERDUE_FINE_TYPE', 'CUSTOMER_DEFINED_02'); // Alma fine code
define('PROCESS_FEE_TYPE', 'CUSTOMER_DEFINED_03');  // Alma fee code
define('REPLACEMENT_FEE_TYPE', 'CUSTOMER_DEFINED_04'); // Alma feecode
define('WAIVE_REASON', 'LOSTITEMWASFOUND'); // Alma reason for waiving fees
define('FINE_OWNER', 'XXXXX'); // Alma Library Code for your institution
define('LIBCAL_BARCODE_LABEL', 'CallNum'); // we store the equip item call number in the LibCal barcode field
define('ERR_LOG_PREFIX', 'Equipment Fines');  // String pre-pended to error messages to identify this script

// array with Alma custom fine/fee type codes as keys and descriptions as values
define('FINE_DESCRIPTION_ARRAY',array(
	OVERDUE_FINE_TYPE => "Equipment overdue fine",
	PROCESS_FEE_TYPE => "Equipment lost item process fee",
	REPLACEMENT_FEE_TYPE => "Equipment lost item replacement fee"
	));
	
// Define Time intervals used in processing 

/* Production date calculation values */
define('FINE_DAYS_INTERVAL', 'P1D'); // php time interval for matching against fine date (match date one day before current date)
define('DAYS_TO_LOST', '-29'); // number of days from due date when item is declared lost
define('LOST_DAYS_INTERVAL',  'P' . abs(DAYS_TO_LOST) . 'D'); // php time interval for calculating date declared lost
define('DAYS_TO_LOST_WARNING', (int)DAYS_TO_LOST + 3); // number of days from due date when item lost warning is sent
define('FINE_DATE_STRING_FORMAT', 'd-m-Y H:i'); // date format used for reports and comments
define('GRACE_PERIOD_MINUTES', 16); // Fines less than GRACE_PERIOD_MINUTES overdue will not be posted to Alma




//define('BARCODE_PREFIX', '32047'); // prefix for all standard MRU Library barcodes ( not currently used, but included for possible future use)

// Email settings
define('EMAIL_DOMAIN', '@XXXX.ca'); // also used in main script for 
define('NOTICE_EMAIL_SENDER', 'XXXXXXX' . EMAIL_DOMAIN); // address for email sender of patron notices
define('REPORT_EMAIL_RECIPIENTS', 'XXXXXXX' . EMAIL_DOMAIN); // address for processing report recipient
/
define('REPORT_EMAIL_SENDER', 'XXXXXXX' . EMAIL_DOMAIN); // address for email sender of processing report

?>