<?php
/******************************************************************************* 
	Main script for LibCal fines export to Alma 
	- posts overdue fines to Alma next day after they are assessed in LibCal
	- fines skipped if overdue less thant GRACE_PERIOD_MINUTES
	- assumes items overdue in LibCal for more than 28 days are lost and posts fines and fees to Alma
	- assumes items checked into LibCal after 28 days are found & waives posted replacement fees in Alma
	- sends email notification to patrons 3 days before a LibCal overdue item is assumed lost
	- sends a daily email report to library staff summarizing posted fines, patron emails, and any errors
	  encountered during processing.
	- IMPORTANT: the body of the Fine->setUserInfo() method needs to be defined for your institution. This method
		needs to confirm that the patron in LibCal has a valid account in Alma and then populate the patron's name and ALMA
		identifier in the Fine object. How this is accomplished depends on what identifiers your institution uses in both LibCal and Alma and what tools you can use to validate and retrieve the information from Alma. In our case,
		we make a call to our Student Information System to get the name and the identifier for Alma.
	@author mgauthier@mtroyal.ca
	@updated 2020-03-02 by MG: 
		- function getMinutesBetweenDates added to address LibCal API bug with fine date (UTC string is 7 hours ahead) NOTE: This has not been tested recently so may have been resolved in a LibCal release.
		@updated 2021-12-01 by MG: Revised processOverdueFines function to post simple overdue fines to Alma with LibCal fine details even if LibCal API call to retrieve additional item details fails.
		Fixed bad variable name in Fine->copyFine method. 
		Removed one of the html encoded spaces from the $tab string in the PatronFineGroup->OutputGroup method. 
		Removed EQUIP_ERROR_LOG_FILE constant definition (relocated to config file).
*******************************************************************************/
	
	 /** SET SERVER PATH VARIABLES DEPENDING ON WHETHER THIS IS A PRODUCTION OR DEVELOPMENT SCRIPT **/
	define('PROD_FILE_PATH', '/xxxx/xxxx/xxxx');
	define ('PREP_FILE_PATH', '/xxxx/xxxx/xxxx');
	$is_development_script = false;  /*** IMPORTANT: set to false for production script ***/
	
	if ($is_development_script){
		define('LIB_FILE_PATH', PREP_FILE_PATH);
	} else {
		define('LIB_FILE_PATH', PROD_FILE_PATH);
	}
	
	/* INCLUDE FILES */	
	include (LIB_FILE_PATH . "equipFinesConfig.php"); // config file containing constants and key variables values
	include (LIB_FILE_PATH ."equipment/equipEmail.php"); // contains the text for the email notices
	include (LIB_FILE_PATH . "libCalAPI_Utils.php"); // contains routines for retrieving json data from the LibCalAPI
	
	include (LIB_FILE_PATH . "sendEmail.php"); // contains routines for sending emails

	define('EQUIP_ERROR_LOG_FILE', DATASTORE_PATH . "fines_script_error.log"); // error log file for equip Fines script

	// constants for formatting output for debugging and reporting
	define("LINEBREAK", "<br>");
	define("DOUBLE_LINEBREAK", "<br><br>");
	define("SECTIONBREAK", "<hr><br>");

	// variable declarations	
	$accessToken = "";
	$overdueFinesArray = array(); // holds Fine objects for overdue fines successfully processed
	$lostItemsArray = array();  // holds Fine objects for lost items successfully processed
	$waivedFeesArray = array(); // holds Fine objects for items whose lost fees were successfully waived
	$lostWarningArray = array(); // holds Fine objects for items for which lost item warning emails were sent
	$fineErrorArray = array(); // holds Fine objects that could not be posted to Alma to due errors
	$otherErrorArray = array(); // holds error messages for other errors encountered during processing (e.g., API call errors)
	$reportWarningArray = array(); // holds warning messages for missing details for inclusion in the processing report

	$currentDate = new DateTime();
	
	// create date object to limit fine processing to fines levied on a given date
	$dateToCheck = new DateTime(); // 
	$dateToCheck->sub(new DateInterval(FINE_DAYS_INTERVAL));
	
	// get the access token for the LibCal API
	try{
		$accessToken = getToken(EQUIP_TOKEN_PHP_FILE, EQUIP_MAX_TOKEN_AGE, $creds);
	} catch(Exception $e){
		logError(ERR_LOG_PREFIX . " Get Access Token " . $e->getMessage(), EQUIP_ERROR_LOG_FILE);
		$jobReportBody = "ERROR retrieving LibCal access token. Cannot execute API calls for Equipment Fines.";
	}
			
	// if valid LibCal token, proceed with API calls
	if(! empty($accessToken)){
		// retrieve and process outstanding fines for the current date
		try{ 
			$API_fines_url = $creds["url"] . "equipment/fines?status=outstanding";
			$fines_response = getAPIData($API_fines_url, $accessToken);
			
			// process the results of the outstanding fines call
			$overdueFinesArray = processOverdueFines($fines_response, $dateToCheck, $almaUsersAPIUrl, $almaKey, $creds["url"], $accessToken, $fineErrorArray, $waivedFeesArray);
		} catch (Exception $e){
			$errorMessage = ERR_LOG_PREFIX . " LibCal Get all oustanding fines call " . $e->getMessage();
			logError($errorMessage, EQUIP_ERROR_LOG_FILE);
			array_push($otherErrorArray, $errorMessage);
		}
		
		// retrieve and process long overdue items
		try {
			$API_overdue_url = $creds["url"] . "equipment/overdue";
			$overdueResults = getAPIData($API_overdue_url, $accessToken);
			$lostItemsArray = processLongOverdueItems($overdueResults, $currentDate, $almaUsersAPIUrl, $almaKey, $creds["url"], $accessToken, $lostWarningArray, $fineErrorArray);			
			
		} catch (Exception $e){
			$errorMessage = ERR_LOG_PREFIX . " Long overdue items " . $e->getMessage();
			logError($errorMessage, EQUIP_ERROR_LOG_FILE);
			array_push($otherErrorArray, $errorMessage);
		}
	} else { // access token empty so log error
				$errorMessage = ERR_LOG_PREFIX . "ERROR: Empty LibCal access token found." . $e->getMessage();
				logError($errorMessage, EQUIP_ERROR_LOG_FILE);
				array_push($otherErrorArray, $errorMessage);

	}// end not empty access token	

	generateReport($dateToCheck, $overdueFinesArray, $lostItemsArray, $waivedFeesArray, $lostWarningArray, $fineErrorArray, $otherErrorArray, $reportWarningArray);

/***************************************************************************************************
 * Purpose: generates and sends an email report of the fines and or fees processed  
 * @param DateTime object $matchDate containing date the LibCal fines were assessed. 
 * @param array $overdueFinesArray a reference to an array of Fine objects for the 'Equipment overdue fine' type.
 * @param array $lostItemsArray a reference to an array of Fine objects for assessed when item is assumed lost.
 * @param array $waivedFeesArray a reference to an array of Fine objects for waived 'Equipment lost item replacement fees'.
 * @param array $lostWarningArray a reference to an array of Fine objects for patrons receiving lost warning notices.
 * @param array $fineErrorArray a reference to an array of Fine objects that could not be assessed to due errors.
 * @param array $otherErrorArray a reference to an array of strings containing reports of other processing errors.
 * @param array $warningArray a reference to an array of Fine objects for fines that were processed successfully but which
 *              contain missing Fine property values.
 * Effects: Compiles and formats a the contents of the given arrays into a report that is emailed to designated recipients
 *          defined in the REPORT_EMAIL_RECIPIENTS constant
  **************************************************************************************************/
function generateReport($matchDate, &$overdueFinesArray, &$lostItemsArray, &$waivedFeesArray, &$lostWarningArray, &$fineErrorArray, &$otherErrorArray, &$warningArray){

	$jobReportBody = "";	
	$jobSubject = "LibCal/Alma Fine Processing Job run on " . date_format(new DateTime(), 'Y-m-d H:i');

	if(count($overdueFinesArray) > 0){
		$jobReportBody =  "<strong>Overdue Fines Posted from " . date_format($matchDate, 'Y-m-d') . " (" . count($overdueFinesArray) . "):</strong>" . DOUBLE_LINEBREAK;
		$jobReportBody .= displayFineArrayOutput($overdueFinesArray, $warningArray);
	} else {
		$jobReportBody .= "No equipment overdue fines posted to Alma." . DOUBLE_LINEBREAK;
	}

	if(count($lostItemsArray) > 0){
		$jobReportBody .= SECTIONBREAK;
		$jobReportBody .= "<strong>Lost Items Fines and Fees Posted:</strong>" . DOUBLE_LINEBREAK;
		$jobReportBody .= displayFineArrayOutput($lostItemsArray, $warningArray);				
	} else {
		$jobReportBody .= SECTIONBREAK;		
		$jobReportBody .= "No equipment lost item fees posted to Alma." . DOUBLE_LINEBREAK;
	}

	if(count($waivedFeesArray) > 0){
		$jobReportBody .= SECTIONBREAK;
		$jobReportBody .= "<strong>Lost Item Replacement Fees Waived:</strong>" . DOUBLE_LINEBREAK;
		$jobReportBody .= displayFineArrayOutput($waivedFeesArray, $warningArray);
	} else {
		$jobReportBody .= SECTIONBREAK;		
		$jobReportBody .= "No lost item replacement fees waived." . DOUBLE_LINEBREAK;
	}
	
	if(count($lostWarningArray) > 0){
		$jobReportBody .= SECTIONBREAK;
		$jobReportBody .= "<strong>Soon-To-Be-Declared Lost Emails:</strong>" . DOUBLE_LINEBREAK;
		$jobReportBody .= displayFineArrayOutput($lostWarningArray, $warningArray);
	} else {
		$jobReportBody .= SECTIONBREAK;		
		$jobReportBody .= "No warning emails sent." . DOUBLE_LINEBREAK;
	}


	if(count($warningArray) > 0){
		$jobReportBody .= SECTIONBREAK;
		$jobReportBody .= "<strong>Warnings:</strong>" . DOUBLE_LINEBREAK;
		$emptyArray = array();
		$jobReportBody .= displayFineArrayOutput($warningArray);
	} 

	$fineErrorCount = count($fineErrorArray);
	$otherErrorCount = count($otherErrorArray);
	$totalErrors = $fineErrorCount + $otherErrorCount;
	if($totalErrors > 0){
		$jobReportBody .= SECTIONBREAK;
		$jobReportBody .= "<strong>Errors:</strong>" . DOUBLE_LINEBREAK;

		if($fineErrorCount > 0){
			$jobReportBody .= displayFineArrayOutput($fineErrorArray);
		}
		if($otherErrorCount > 0){
			foreach($otherErrorArray as $errorMsg){
				$jobReportBody .= $errorMsg . DOUBLE_LINEBREAK;
			}
		}
	} else {
		$jobReportBody .= SECTIONBREAK;
		$jobReportBody .= "No errors found during processing." . DOUBLE_LINEBREAK;
	}
		
	sendMail(EQUIP_ERROR_LOG_FILE, REPORT_EMAIL_SENDER, REPORT_EMAIL_RECIPIENTS, $jobSubject, $jobReportBody);
	/*********** output report text for debugging 
	echo $jobSubject . SECTIONBREAK;
	echo $jobReportBody;
	***************************************/
		
}

/***************************************************************************************************
 * Purpose: loops through the given array $fineArray and returns a string containing select fine details
 *          grouped by patron and item.*         
 * @param array $fineArray a reference to an array of Fine objects
 * @param [optional] array $warnings reference to array to hold Fine objects that have non-empty warning_message properties;
 * @return string containing the formatted and grouped details
 * Effects: if $warnings array is supplied as param, loads any Fine objects from the $fineArray into the $warnings
 *          array if the Fine object has a non-empty warning_message property.
  **************************************************************************************************/
function displayFineArrayOutput(&$fineArray, &$warnings = null){
	
	$output = "";
	$patronArray = array(); // array to group fines by patron email
	foreach($fineArray as $fineObj){			
		$patronEmail = $fineObj->getPatronEmail();
		
		if(array_key_exists($patronEmail, $patronArray)){
			// add to existing patron group object
			$groupObj = $patronArray[$patronEmail];
			$groupObj->addItem($fineObj);
		} else {
			// add new key for new patron group object
			$patronArray[$patronEmail] = new PatronFineGroup($fineObj);
		}
		// add fine object to warning array if it has an warning message, even though it may have been processed successfully
		if(! is_null($warnings) && $fineObj->hasWarning()){
			array_push($warnings, $fineObj);
		}
	}
	
	foreach ($patronArray as $pKey => $group){
		$output .= $group->outputGroup();
	}

	return $output;
}


/***************************************************************************************************
 * Purpose: parse Alma fine results fo find the fine ID for the fine that contains the given LibCal item ID 
 *          in the Alma fine comment field and the LibCal item value in the Alma fine orginal amount field.
 * @param string $results contains Alma fines as json
 * @param string $itemId contains the LibCal item id for an item
 * @param string $itemValue contains the LibCal replacement value for the item
 * @return string containing the Alma feed ID number for the matching Alma fine, 
 *         if no match found, returns and empty string.
  **************************************************************************************************/
function getAlmaFeeId($results, $itemId, $itemValue){

	$json_obj = json_decode($results, true);
	
	if($json_obj["fee"]){ // extract fee array from result string
		$feesArray = array($json_obj["fee"]);	
		// loop through the fines 
		
		$value = (float)$itemValue; // convert value to float for comparison
		foreach($feesArray as $feeList){
			// loop through each individual fee array
			foreach($feeList as $fee){
				// extract fee details
				$feeId = $fee['id'];
				$comment = $fee['comment']; // Alma fee comment field value
				$amount = $fee['original_amount']; // Alma fee amount value
				
				$type = $fee['type']['value'];
				
				if($type == REPLACEMENT_FEE_TYPE){
					$strMatch = "|ID:" . $itemId . "|"; // regex pattern
					// if the item id is in the comment field and the item value equals the fine orginal value
					if(preg_match($strMatch, $comment) == 1 && $value == (float)$amount){	
						return $feeId;
					}
				}
			}
		}
	}
	return ""; // Alma fee id not found so return empty string
}	

/**********************************************************************************
 * Purpose: Compares given date string with given date object and returns true if they fall on the same day
 * @param string containing a date in the format: 2017-04-06T07:30:00-06:00,
 * @matchDate DateTime object for the date to compare to the given date string
 * @return true if given date-time string falls on the same day as the DateTime object; false otherwise
 ***********************************************************************************/ 	
function checkFineDate($fineDateStr, $matchDate){		
	
	$fineDate = DateTime::createFromFormat("Y-m-d\TH:i:sT", $fineDateStr); // create dateTime object
	// zero out time so we can compare the day only
	$matchDate->setTime(0,0,0);  // set time to zero for comparison
	$fineDate->setTime(0,0,0);

	if($matchDate == $fineDate){
		return true;
		
	} else {
		return false;
	}	
}	
/**********************************************************************************
 * Purpose:Returns the number of days difference between  the given date string and the given DateTime object
 * @param string containing a date in the format: 2017-04-06T07:30:00-06:00,
 * @param DateTime $dateObj a DateTime object
 * @return a signed integer representing the number of days difference: Integer will be zero if less than 1 day; positive if date object is greater than the given string, negative if it is less. 
 ***********************************************************************************/ 	
function checkDateDiff($dateStr, $dateObj){
	
	$dateToCompare = DateTime::createFromFormat("Y-m-d\TH:i:sT", $dateStr); // create dateTime object
	// zero out time so we can compare the day only
	$dateObj->setTime(0,0,0);  // set time to zero for comparison
	$dateToCompare->setTime(0,0,0);

	return (int)date_diff($dateObj, $dateToCompare)->format('%r%a') ;
}	

/**********************************************************************************
 * Purpose: Returns the number of minutes between two date strings
 * @param string $dateStr1 containing a date in the format: 2017-04-06T07:30:00-06:00,
 * @param string $dateStr2 containing a date in the format: 2017-04-06T07:30:00-06:00,
 * @return int value for difference between the two date strings
 ***********************************************************************************/ 	
function getMinutesBetweenDates($string1, $string2){
	$interval = strtotime($string1) - strtotime($string2);
	return (int)$interval/60;
	
}

/*****************************************************************************
 * Purpose: Add the given time amount to the given UTC_string time string
 * @param integer $timeAmount in second
 * @param string $UTC_string contains a string in UTC date format
 * @return string containing the revised UTC string with the time added
 ******************************************************************************/
function adjustUTCString($timeAmount, $UTC_string){
	
	$time = strtotime($UTC_string) + $timeAmount;
	
	return date("Y-m-d\TH:i:sP", $time);
	
}

/**********************************************************************************
 * Purpose: processes the results of the LibCal oustanding fines API call to find any fines 
 *          levied on the given $matchDate then uses further API calls to LibCal and Alma populate a Fine object
 *          and load it into into Alma.
 * @param string $fines a string containing the json results from the LibCal fines API call
 * @param DateTime $matchDate a DateTime object
 * @param string $almaUrl the base url for the Alma API call
 * @param string $almaKey the API key for Alma
 * @param string $libCalUrl the base url for the LibCal API call
 * @param string $accessToken the token for the LibCal API
 * @param array &$errors reference to array that contains error messages
 * @param array &$waivesArray reference to array that contains successfully waived fee Fine objects
 * @return: An array of fine objects that were successfully posted to Alma; may be empty.
 ***********************************************************************************/
function processOverdueFines($fines, $matchDate, $almaUrl, $almaKey, $libCalUrl, $token, &$errors, &$waivesArray){
	
	$fineArray = array();
	$json_obj = json_decode($fines, true);	
	
	if(is_array($json_obj)){
		// loop through the fines
		foreach($json_obj as $fine){	
			$fineDate = $fine['fine_date'];
			$fineLocation = $fine['booking']['lid'];
			$locationFlag = false;
			foreach(LIBCAL_LOCATIONS as $loc){
				if($loc == $fineLocation){
					$locationFlag = true;
					break;
				}
			}
			// adjust fine date to account for API bug that adds 7 hours (25200 seconds) to the fine date
			$fineDate = adjustUTCString(-25200, $fineDate);
			
			// only want to process fines levied if location id is in the location array and fine date matches given matchdate
			if($locationFlag && checkFineDate($fineDate, $matchDate)){	

				$dueDate = $fine['booking']['due_date'];
				$overdueMinutes = getMinutesBetweenDates($fineDate, $dueDate);

				// only process fine if overdue more than the grace period
				if($overdueMinutes > GRACE_PERIOD_MINUTES){
					
					// create a fine object
					$fineObj = new Fine(
						$fine['fine_id'],
						$fine['fine_amount'],
						$fineDate, 
						$fine['booking']['cid'],
						$fine['booking']['email'],
						$fine['booking']['id'],
						$fine['booking']['item_name'], 
						$dueDate,
						OVERDUE_FINE_TYPE
					);
					
					/*** TEST PATRON OVERRIDE - COMMENT OUT BEFORE PRODUCTION IMPLEMENTATION 
					$fineObj->setPatronId("000000000"); // enter valid Alma test patron ID
					$fineObj->setPatronEmail("testemail@XXXX.XX"); // enter valid email address
					$fineObj->setPatronName("Student Test");
					 END TEST CODE ******************************/
					
					// check if fineObj was able to load the institutional Id for the patron
					if($fineObj->getPatronId()){ 
						// Use the LibCal API to get the item barcode and replacement value
						$API_item_url = $libCalUrl . "equipment/item/" . $fineObj->getItemId();
						
						try{
							// get item details from LibCal
							$equipResult = getAPIData($API_item_url, $token);
							$jsonEquipObj = json_decode($equipResult, true);
							
						} catch (Exception $e){
							$msg = "ERROR: Could not retrieve item details";
							$fineObj->setWarningMessage($msg);
							logError(ERR_LOG_PREFIX . $msg .  " Item ID:" . $fineObj->getItemId() . " " . $e->getMessage(), EQUIP_ERROR_LOG_FILE);
							//array_push($errors, $fineObj);
						}
						if(is_array($jsonEquipObj) && count($jsonEquipObj) > 0){ 
							foreach($jsonEquipObj as $equipment){
								if(isset($equipment['error'])){
									// item ID not found so add warning
									$fineObj->setWarningMessage("Overdue fine processed but could not find item ID in LibCal.");
								} else {
									// include additional item details in the post
									$fineObj->setItemBarcode($equipment['barcode']);
									$fineObj->setItemValue($equipment['value']);
								}
							}
						}
						
						// check if item has been overdue long enough to have been declared lost
						$negativeLongOverdueDiff = (int)DAYS_TO_LOST;
						$daysSinceDueDate = checkDateDiff($fineObj->getDueDate(), $matchDate);
						
						if($daysSinceDueDate <= $negativeLongOverdueDiff ){	
							// assume it's a long-overdue/assumed-lost item and post refund of item value to Alma
							$fineObj->setFineCode(WAIVE_REASON);
							$fineObj->setFineAmount($fineObj->getItemValue());
							// get all outstanding fines for the patron from Alma
							$API_retrieve_fines_url = $almaUrl . $fineObj->getPatronId() . "/fees?status=ACTIVE&apikey=" . $almaKey;
							
							try{
								$retrievedFinesResults = curlGet($API_retrieve_fines_url);
								// parse the Alma fines and get the Alma fine ID for the item replacement fee
								$almaFeeId = getAlmaFeeId($retrievedFinesResults, $fineObj->getItemId(), $fineObj->getItemValue());
								
								if($almaFeeId){
									//waive fine in alma
									$waiveComment = rawurlencode("Item checked into LibCal equipment on " . getFormattedDateFromUTC($fineDate, FINE_DATE_STRING_FORMAT));
									
									try{
										$API_waive_fine_url = $almaUrl . $fineObj->getPatronId() . "/fees/" . $almaFeeId . "?op=waive&amount=" . $fineObj->getItemValue() . "&reason=" . WAIVE_REASON . "&comment=" . $waiveComment . "&apiKey=" . $almaKey;
										
										$waiveResults = curlPost($API_waive_fine_url);
										
										array_push($waivesArray, $fineObj);
										
									} catch (Exception $e){
										$msg = "ERROR: Could not waive item replacement fee for Alma fee ID# $almaFeeId. ";
										$fineObj->setWarningMessage($msg);
										logError(ERR_LOG_PREFIX . $msg .  $e->getMessage(), EQUIP_ERROR_LOG_FILE);
										array_push($errors, $fineObj);
									}
								} else {
									$fineObj->setWarningMessage("ERROR: Could not waive item replacement fee because matching replacement fee not found in Alma.");
									array_push($errors, $fineObj);
								}
							} catch(Exception $e) {
								$msg = "ERROR: Could not waive lost item fee in Alma. ";
								$fineObj->setWarningMessage($msg);
								logError(ERR_LOG_PREFIX . $msg .  $e->getMessage(), EQUIP_ERROR_LOG_FILE);
								array_push($errors, $fineObj);
							}
						} else { //Not a long-overdue-item so post regular overdue fine to Alma
							$jsonPayload = $fineObj->createAlmaFinePayload(true); // create the json payload for the fine
							
							try{
								$fineUrl = $almaUrl . $fineObj->getPatronId() . "/fees?apikey=" . $almaKey;
								$postResult = curlPost($fineUrl, $jsonPayload);
								
								array_push($fineArray, $fineObj);
							} catch(Exception $e){
								$msg = "ERROR: Could not post overdue fine to Alma. ";
								$fineObj->setWarningMessage($msg);
								logError(ERR_LOG_PREFIX . $msg .  $e->getMessage(), EQUIP_ERROR_LOG_FILE);
								array_push($errors, $fineObj);
								
							}
						}
					} else { // no institutional Id for the patron so load the fine object into the error array
						$fineObj->setWarningMessage("ERROR: Could not post overdue fine: Patron Id for patron not found.");
						array_push($errors, $fineObj);
					}
				} // end if item is less than 15 minutes overdue 
			} // end if location ID and match date 
		} // end json object loop
	}

	return $fineArray;	
}

	
/**********************************************************************************
 * Purpose: loop through the json results for the overdue item API call and post fines to Alma
 *          for any items that are exactly overdue by the value of the DAYS_TO_LOST global constant
 * @param string $overdueResults containing json results of the overdue items API call,
 * @param DateTime $dateObj a DateTime object containing the date to compare against
 * @param string $almaUrl containing the base url for the Alma API
 * @param string $almaKey containing the API key for Alma
 * @param string $libCalUrl containing the base url for the LibCal equipment API
 * @param string $token containing LibCal API token string
 * @param array $warningEmailArray reference to array that contains Fine objects for lost warning letters
 * @param array string $errors reference to array that contains error messages
 * @return: An array of fine objects that were successfully posted to Alma; may be empty.
 ***********************************************************************************/ 	
function processLongOverdueItems($overdueResults, $matchDate, $almaUrl, $almaKey, $libCalUrl, $token, &$warningEmailArray, &$errors){
	
	$overdueArray = array();
	$json_obj = json_decode($overdueResults, true);
	
	if(is_array($json_obj)){
		
		// loop through the fines
		foreach($json_obj as $item){
			$dueDate = $item['due_date'];
			$dateDiff = checkDateDiff($dueDate, $matchDate);
			//echo "DATE DIFF:" . $dateDiff . LINEBREAK;
			$lostFlag = ($dateDiff == DAYS_TO_LOST) ? true : false; // meets the long overdue date criteria
			$warningEmailFlag = ($dateDiff == DAYS_TO_LOST_WARNING) ? true : false; // meets the warning email date criteria
			
			$fineLocation = $item['lid'];
			foreach(LIBCAL_LOCATIONS as $loc){
				if($loc == $fineLocation){
					$locationFlag = true;
					break;
				}
			}
			
			// if location id in location array and due date is exactly DAYS_TO_LOST days from today, 
			// then assume item is lost and post fines and fees
			if($locationFlag && ($lostFlag || $warningEmailFlag)){
				$errMessage = ($lostFlag) ? "Did not post to Alma." : "Did not send warning letter.";
				$postDate = date("Y-m-d\TH:i:sP"); // current date/time in UTC format
				$itemId = $item['id'];
				$email = $item['email'];
				
				/* create a fine object */
				$fineObj = new Fine(
					"", // blank fine ID 
					"", // blank fine amount (will be populated in later)
					$postDate,
					$item['cid'],
					$item['email'],
					$item['id'],
					$item['item_name'], 
					$dueDate
				);
				/*** TEST PATRON OVERRIDE - COMMENT OUT BEFORE PRODUCTION IMPLEMENTATION 
				$fineObj->setPatronId("000000000"); // enter valid Alma test patron ID
				$fineObj->setPatronEmail("testemail@XXXX.XX"); // enter valid email address
				$fineObj->setPatronName("Student Test");
				 END TEST CODE ******************************/

				// check if fineObj was able to load the patron Id
				if($fineObj->getPatronId()){ 
					// Use the LibCal API to get the item barcode and replacement value
					$API_item_url = $libCalUrl . "equipment/item/" . $fineObj->getItemId();
					
					try{
						$equipResult = getAPIData($API_item_url, $token);
						$jsonEquipObj = json_decode($equipResult, true);
						
						foreach($jsonEquipObj as $equipment){
							$fineObj->setItemBarcode($equipment['barcode']);
							$fineObj->setItemValue($equipment['value']);
						}
						
						if(! empty($fineObj->getItemValue())){							
							$categoryId = $fineObj->getItemCategory();
							
							// get max fine amount based on item category Id
							$feesArray = (array_key_exists($categoryId, CATEGORY_FEES)) ? CATEGORY_FEES[$categoryId] : DEFAULT_FEES_ARRAY;
							$maxFine = $feesArray['MaxFine'];

							// create Multi-dimensional array with fine types and amounts for the 3 fines to be posted for each item
							$lostItemFeesArray = array(
								array("type" => OVERDUE_FINE_TYPE, "amount" => $maxFine),
								array("type" => PROCESS_FEE_TYPE, "amount" => PROCESS_FEE_AMOUNT),
								array("type" => REPLACEMENT_FEE_TYPE, "amount" => $fineObj->getItemValue())
							);
							
							$postedFeesArray = array();
							$warningFeesArray = array();
							
							// for each item post 3 fines/fees
							for($i=0; $i<3; $i++){
								
								$fineCopy = $fineObj->copyFine();
								
								$fineCopy->setFineCode($lostItemFeesArray[$i]["type"]);
								$fineCopy->setFineAmount($lostItemFeesArray[$i]["amount"]);
								
								if($lostFlag){
									// format the fine details as a json string for uploading 
									$jsonPayload = $fineCopy->createAlmaFinePayload(false); 
																	
									//construct the url for the API call
									$fineUrl = $almaUrl . $fineCopy->getPatronId() . "/fees?apikey=" . $almaKey;
									
									try{
										$postResult = curlPost($fineUrl, $jsonPayload);
										array_push($overdueArray, $fineCopy);
										array_push($postedFeesArray, array($fineCopy->getFineCodeDescription(), $fineCopy->getFineAmount()));
										
										
									} catch(Exception $e){
										$msg = "ERROR: Could not post fee " . $lostItemFeesArray[$i]["type"] . " to Alma. ";
										$fineCopy->setWarningMessage($msg);
										logError(ERR_LOG_PREFIX . $msg .  $e->getMessage(), EQUIP_ERROR_LOG_FILE);
										array_push($errors, $fineCopy);
									}
								} else if ($warningEmailFlag){
									array_push($warningFeesArray, array($fineCopy->getFineCodeDescription(), $fineCopy->getFineAmount()));
								}									
							}
							if(count($postedFeesArray) > 0){
								// send email notification to patron 
								$emailSubject = "Long Overdue - Declared Lost";
								$emailBody = generateLostEmailBody($fineObj->getPatronName(), $fineObj->getItemName(), getFormattedDateFromUTC($fineObj->getDueDate(), FINE_DATE_STRING_FORMAT), $emailSubject, $postedFeesArray);
								
								if(! $emailResult = sendMail(EQUIP_ERROR_LOG_FILE, NOTICE_EMAIL_SENDER, $fineCopy->getPatronEmail(), $emailSubject, $emailBody)){
									$fineObj->setWarningMessage("ERROR sending long-overdue-assumed-lost email to patron.");
									array_push($errors, $fineObj);
								}
								
							}
							if(count($warningFeesArray) > 0){
								// send advance warning email notification to patron 
								$emailSubject = "Long Overdue - Notification";
								$lostDate = DateTime::createFromFormat("Y-m-d\TH:i:sP",$fineObj->getDueDate());
								$lostDate->add(new DateInterval(LOST_DAYS_INTERVAL));
								$lostDateStr = date_format($lostDate, "M. j, Y");
								$emailBody = generateWarningEmailBody($fineObj->getPatronName(), $fineObj->getItemName(), getFormattedDateFromUTC($fineObj->getDueDate(), FINE_DATE_STRING_FORMAT), $lostDateStr, $emailSubject, $warningFeesArray);
								
								if(! $emailResult = sendMail(EQUIP_ERROR_LOG_FILE, NOTICE_EMAIL_SENDER, $fineObj->getPatronEmail(), $emailSubject, $emailBody)){
									$fineObj->setWarningMessage("ERROR sending long-overdue-notification email to patron.");
									array_push($errors, $fineObj);
								} else {
									array_push($warningEmailArray, $fineObj);
								}
								
							}
						} else {
							$fineObj->setWarningMessage("ERROR: Lost item missing value in LibCal. " . $errMessage);
							array_push($errors, $fineObj);
						}
					} catch (Exception $e){
						$msg = "ERROR: Could not retrieve LibCal item details. ";
						$fineObj->setWarningMessage($msg);
						logError(ERR_LOG_PREFIX . $msg .  $e->getMessage(), EQUIP_ERROR_LOG_FILE);
						array_push($errors, $fineObj);
					}
				} else { // no institutional Id for the patron so load the fine object into the error array
					
					$fineObj->setWarningMessage("ERROR: Could not find Institutional Id for user." . $errMessage);
					array_push($errors, $fineObj);
				}
			} // end check locationFlag && date 
		} // end json object loop
	}
	return $overdueArray;
}
						

/*****************************************************************************
 * Purpose: Use CURL to post to given url with optional data object 
 * @param string $url the url to post to
 * @param array $data [optional] optional data object to post
 * @return string containing the response to the CURL post
 * @throws an exception if the API call is unsuccessful
 ******************************************************************************/
function curlPost($url, &$data = null){	
	$crl = curl_init();
	curl_setopt($crl, CURLOPT_URL, $url);
	curl_setopt($crl, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
	
	$headerArray = array('Content-Type: application/json');
	if($data){
		array_push($headerArray, 'Content-Length: ' . strlen($data));
		curl_setopt($crl, CURLOPT_POSTFIELDS, $data);
	}
	
	curl_setopt($crl, CURLOPT_HTTPHEADER, $headerArray);

	$curl_response = curl_exec($crl);
	$status = curl_getinfo($crl, CURLINFO_HTTP_CODE);	
	
	if ($status != 200) {
		$msg = "ERROR: call to URL $url failed with status $status, response $curl_response, curl_error ";
		throw new Exception( $msg .  curl_error($crl) . ", curl_errno " . curl_errno($crl) . "\n");
	}
	curl_close($crl);
	
	return $curl_response;

}

/*****************************************************************************
 * Purpose: Use CURL for GET call to given url
 * @param string $url the url to post to
 * @return string containing the response to the CURL post
 * @throws an exception if the API call is unsuccessful
 ******************************************************************************/

function curlGet($url){
	$curl_response = null;
	
	$crl = curl_init();
	curl_setopt($crl, CURLOPT_URL, $url);
	curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($crl, CURLOPT_HTTPGET, 1);
	$headerArray = array('Accept: application/json');
	curl_setopt($crl, CURLOPT_HTTPHEADER, $headerArray);


	$curl_response = curl_exec($crl);
	$status = curl_getinfo($crl, CURLINFO_HTTP_CODE);	
	
	if ($status != 200) {
		throw new Exception("ERROR: call to URL $url failed with status $status, response $curl_response, curl_error " . curl_error($crl) . ", curl_errno " . curl_errno($crl) . "\n");
	}
	curl_close($crl);

	return $curl_response;	
}


// Define Fine object class
	class Fine{
		private $fine_id = ""; // LibCal unique fine id
		private $fine_amount = ""; // amount of fine
		private $fine_date = null; // date of fine in UTC format string 
		private $patron_email = null; // email address used for equipment booking
		private $patron_id = null;    // institutional Id for user (if found)
		private $patron_name = "";  // first & last name for user (for letter greeting)
		private $item_id = ""; // LibCal equipment item id
		private $item_name = ""; // LibCal equipment item name
		private $item_barcode = ""; // LibCal equipment item barcode
		private $item_value = ""; // replacement cost if item lost or damaged
		private $item_due_date = ""; // string containing date item was due in UTC format
		private $item_category = null; // category ID of fine
		private $warning_message = "";
		private $fine_code = ""; // Alma fine code
		
		
		/**************************************************************************************
		 * Purpose: Fine class object constructor
		 * @param string $fineId contains the unique ID for the fine
		 * @param string $fineAmount contains the amount of the fine
		 * @param string $itemCategory contains the category ID of the fine
		 * @param string $fineDate contains a UTC string for the fine date
		 * @param string $finePatronEmail contains the email address for the patron
		 * @param string $fineItemId contains the LibCal item id
		 * @param string $fineItemName contains the item name
		 * @param string $fineDueDate contains the date the item is due
		 * @param [optional] string $fineCode contains the string for the fine type code, defaults to null
		 * @param [optional] string $patronId contains the string for user id number, defaults to null
		 * @param [optional] string $patronName contains the string for user first & lastname, defaults to null
		 * @param [optional] string $fineBarcode contains the string for LibCal barcode field, defaults to null
		 * @param [optional] string $fineBarcode contains the string for LibCal barcode field, defaults to null
		 * Effects: initializes the class object properties using the given parameters.
		 ***************************************************************************************/		
		function __construct($fineId,$fineAmount, $fineDate, $itemCategory, $finePatronEmail, $fineItemId, $fineItemName, $fineDueDate, $fineCode = null, $patronId = null, $patronName = null, $fineItemValue = null, $fineItemBarcode = null){
			$this->fine_id = $fineId;
			$this->fine_amount = $fineAmount;
			$this->fine_date = $fineDate;
			$this->item_category = $itemCategory;
			$this->patron_email = trim($finePatronEmail);
			$this->item_id = $fineItemId;
			$this->item_due_date = $fineDueDate;
			$this->item_name = $fineItemName;
			$this->fine_code = $fineCode;
			$this->setItemValue($fineItemValue);
			$this->item_barcode = $fineItemBarcode;

			if(!$patronId){				
				$this->setUserInfo();
			} else {
				$this->setPatronId($patronId);
				$this->setPatronName($patronName);
			}
		}

		/* @return a new fine object with the main attributes of this fine object */
		function copyFine(){
			
			$copy = new Fine(
				$this->fine_id,
				$this->fine_amount,
				$this->fine_date, 
				$this->item_category, 
				$this->patron_email, 
				$this->item_id, 
				$this->item_name,
				$this->item_due_date,
				$this->fine_code,
				$this->patron_id,
				$this->patron_name,
				$this->item_value,
				$this->item_barcode
			);
			$copy->setWarningMessage($this->getWarningMessage());
			
			
			return $copy;
		}

		function getItemCategory(){
			return $this->item_category;
		}
		function getFineDate(){
			return $this->fine_date;
		}

		function getDueDate(){
			return $this->item_due_date;
		}
		
		function getPatronId(){
			return $this->patron_id;
		}
		
		function setPatronId($id){
			$this->patron_id = $id;
		}

		function getPatronName(){
			return $this->patron_name;
		}
		
		function setPatronName($name){
			$this->patron_name = $name;
		}
		function setPatronEmail($email){
			$this->patron_email = $email;
		}
		
		
		function getPatronEmail(){
			return $this->patron_email;
		}
		/*  *******************************************************************************
		 * Purpose: return string containing labels and values for non-empty Fine object patron properties 
		 * @ return string
		 *******************************************************************************/
		function getPatronDetails(){
			$output = "";
			$detailsArray = array(
				"Patron Name" => $this->patron_name,
				"Patron Email" => $this->patron_email,
				"Patron ID" => $this->patron_id
			);
			foreach( $detailsArray as $key => $value){
				if($value){
					$output .= $key . ": " . $value . "; ";
				}
			}
			return $output;
		}
		
		function getItemBarcode(){
			return $this->item_barcode;
		}
		
		/******************************************************************************************
		 * Purpose: load the given string into the Fine item_barcode property
		 * @ param string $barcode
		 * Effects: if given string is not null and not empty, load a warning string into the Fine warning_message property
		********************************************************************************************/
		function setItemBarcode($barcode){
			$barcode = trim($barcode);
			if(! is_null($barcode) && empty($barcode)){
				$this->setWarningMessage("No LibCal equipment barcode for this item.");
			}
			$this->item_barcode = $barcode;
		}

		function getItemId(){
			return $this->item_id;
		}

		function getItemName(){
			return $this->item_name;
		}

		function setItemName($name){
			$this->item_name = $name;
		}
		
		function getItemValue(){
			return $this->item_value;
		}
		
		/***************************************************************************************
		* Purpose: set the item_value property with the given value. 
		* If the given value cannot be converted to a number, use default value based on item category.
		* (Default calculated using the constants CATEGORY_FEES and DEFAULT_FEES_ARRAY which are defined
		* in equipFinesConfig.php file)
		* @param string value
		* Effects: if given value is not null and not a number, sets it to a default value and loads a warning message.
		***************************************************************************************/
		function setItemValue($value){
			$val = preg_replace("/[^0-9.]/", "", $value);
			if(is_numeric($val)){
				$val = number_format($val, 2);
				$this->item_value = $val;
			} elseif(! is_null($value)){
				$catId = $this->item_category;
				$itemDefaultValue = (array_key_exists($catId, CATEGORY_FEES)) ? CATEGORY_FEES[$catId]['ReplacementValue'] : DEFAULT_FEES_ARRAY['ReplacementValue'];
				$this->item_value = $itemDefaultValue;
				$this->setWarningMessage("No value available for item: using default value.");
			}
		}

		/***************************************************************************************
		* Purpose: Return a string containing labels and values for select non-empty item properties
		* @return string
		***************************************************************************************/		
		function getItemDetails(){
			$output = "";
	
			$arrayFine = array(
				"Item ID" =>$this->item_id,
				LIBCAL_BARCODE_LABEL => $this->item_barcode,
				"Item Name" => $this->item_name,
				"Due Date" => getFormattedDateFromUTC($this->item_due_date, FINE_DATE_STRING_FORMAT),
				"WARNING" => $this->warning_message
				);
				
			foreach( $arrayFine as $key => $value){
				if($value){
					$output .= $key . ": " . $value . "; "; 
				}
			}
			return $output;
		}
		/***************************************************************************************
		* Purpose: Return a string containing labels and values for select non-empty fine properties
		* @return string
		***************************************************************************************/				
		function getFineDetails(){
			$output = "";
				$arrayFine = array(
				"LibCal Fine ID" => $this->fine_id,
				"Fine Date" => getFormattedDateFromUTC($this->fine_date, FINE_DATE_STRING_FORMAT),
				"Amount" => $this->fine_amount,
				"Fine Type" => $this->getFineCodeDescription()
				);

			foreach( $arrayFine as $key => $value){
				if($value){
					$output .= $key . ": " . $value . "; ";
				}
			}
			return $output;
		}

		function getFineAmount(){
			return $this->fine_amount;
		}
		
		function setFineAmount($value){
			$this->fine_amount = $value;
		}

		function getFineCode(){
			return $this->fine_code;
		}
		
		function setFineCode($value){
			$this->fine_code = $value;
		}
		
		/***************************************************************************************
		* Purpose: Return the description associated with the fine_code property
		* @return string
		***************************************************************************************/		
		function getFineCodeDescription(){
			if($this->fine_code == WAIVE_REASON){
				return "Waive lost item replacement fee";
			} else {
				return FINE_DESCRIPTION_ARRAY[$this->fine_code];
			}
		}
		/***************************************************************************************
		* Purpose: Return true if a non-empty value is set for the warning_message property, false otherwise
		* @return string
		***************************************************************************************/		
		function hasWarning(){
			if(empty($this->getWarningMessage())){
				return false;
			} else {
				return true;
			}
		}
		
		function getWarningMessage(){
			return $this->warning_message;
		}
		/***************************************************************************************
		* Purpose: Concatenate the given $error string to the warning_message property 
		* @param string $msg
		***************************************************************************************/				
		function setWarningMessage($msg){
			if(empty($this->getWarningMessage())){
				$this->warning_message = $msg;
			} else {
				$this->warning_message .= " " . $msg;
			}
		}

		/**************************************************************************************
		* Purpose: Get ALMA patron name and primary identifier  
		* @return: true if user info found, false otherwise.
		* Effects: set the patron_name and patron_id values for the Fine object
		*****************************************************************************************/
		function setUserInfo(){
			$patronName = null;
			$primaryId = null;
			$lookupFound = false;
			/******
			INSERT YOUR CODE HERE FOR LOOKING UP AND POPULATING THE ALMA PRIMARY IDENTIFIER AND PATRON NAME. BASED ON HOW YOU HAVE CONFIGURED LIBCAL, THIS MAY MEAN USING THE PATRON EMAIL ADDRESS OR
			SOME OTHER LIBCAL PATRON IDENTIFIER.
			
			WHEN THE ALMA PRIMARY IDENTIFIER AND PATRON NAME ARE IS RETRIEVED, LOAD THEM INTO THE FINE OBJECT
			*********/	
			if($lookupFound){
				$this->setPatronName(trim($patronName));
				$this->setPatronId($primaryId);					
				return true;
			}
			return false;
		}
		
		/**************************************************************************************
		* Purpose: Return a the string to be used in the Alma fine comment field 
		* @param [optional] bool $addReturnDate defaults to true; if true, adds the item return date 
		*		to the return string. If false, does not add the item return in the return string
		* @return: string containing attribute values from the fine object
		*****************************************************************************************/		
		function generateCommentField(bool $addReturnDate = true){
			
			$itemDetails = "Item ID:" . $this->item_id;
			if($this->item_barcode){
				$itemDetails .= "; " . LIBCAL_BARCODE_LABEL . ":" . $this->item_barcode;
			}
					
			$comment =  "$this->item_name ($itemDetails) due on " . getFormattedDateFromUTC($this->getDueDate(), FINE_DATE_STRING_FORMAT);
			if($addReturnDate){
				$comment .= "; returned on " . getFormattedDateFromUTC($this->getFineDate(), FINE_DATE_STRING_FORMAT);
			}
			return $comment; 
		}
		
		/**************************************************************************************
		* Purpose: Uses the Fine object attributes to generate a string containing the json payload 
		*          parameters for posting a fine to Alma 
		* @param [optional] bool $addReturnDate defaults to true; if true, adds the item return date
		*		to the return string. If false, does not include the item return date in the return string
		* @parm string $fineOwner defaults to the value of the FINE_OWNER constant
		* @return: a json string containing attribute values from the Fine object in the format
		*          required by the Alma "create user fine/fee" API.
		*****************************************************************************************/				
		function createAlmaFinePayload(bool $addReturnDate = true, string $fineOwner=FINE_OWNER){
			
			/* Code for potential future use if we decide to link to Alma barcode ****************** 
			only add barcode to payload barcode field if it matches the standard barcode prefix
			//$almaBarcode = (strcmp(substr($this->item_barcode,0,5),BARCODE_PREFIX)==0)? $this->item_barcode : "";
			***************************************************************************************/
			$almaBarcode = "";
			$fine_json = '{
				  "link": "",
				  "type": {
					"value": "' . $this->getFineCode() . '"
				  },
				  "status": {
					"value": "ACTIVE"
				  },
				  "original_amount": "' . $this->getFineAmount() . '",
				  "creation_time": "' . $this->getFineDate() . '",
				  "comment": "' . $this->generateCommentField($addReturnDate) . '",
				  "owner": {
					"value": "'. $fineOwner . '"
				  },
				  "barcode": {
					"value": "' . $almaBarcode . '"
				  },
				  "transaction": [
					{}
				  ],
				  "bursar_transaction_id": ""
				}
				';
			
			return $fine_json;
		}
		
		/***************************************************************************************
     	 * Purpose: returns a string containing labels and formatted values for the Fine object properties
		 * @param [optional] bool $simplified defaults to true, indicating that the simplified string with fewer property
		 *        details should be returned; if false, then an expanded string of Fine properties is returned.
		 * @return a string containing labels and formatted values for fine attributes
		***************************************************************************************/
		function fineToString(bool $simplified= true){
			$arrayFine= array();
			if($simplified){				
				$arrayFine = array(
				"Patron Email" => $this->patron_email,
				"Patron ID" => $this->patron_id,
				"Patron Name" => $this->patron_name,
				"LibCal Fine ID" => $this->fine_id,
				"Fine Date" => getFormattedDateFromUTC($this->fine_date, FINE_DATE_STRING_FORMAT),
				"Amount" => $this->fine_amount,
				"Fine Type" => $this->getFineCodeDescription(),
				"Item Name" => $this->item_name,
				"Item Barcode" => $this->item_barcode,
				"Due Date" => getFormattedDateFromUTC($this->item_due_date, FINE_DATE_STRING_FORMAT),
				"WARNING" => $this->warning_message
				);
			} else { // for debugging, display more fine properties
				$arrayFine = array(
				"LibCal Fine ID" => $this->fine_id,
				"Due Date" => getFormattedDateFromUTC($this->item_due_date, FINE_DATE_STRING_FORMAT),
				"Fine Date" => getFormattedDateFromUTC($this->fine_date, FINE_DATE_STRING_FORMAT),
				"Amount" => $this->fine_amount,
				"Fine Type Code" => $this->fine_code,
				"Fine Type" => $this->getFineCodeDescription(),
				"Patron Email" => $this->patron_email,
				"Patron ID" => $this->patron_id,
				"Item ID" => $this->item_id,
				"Item Name" => $this->item_name,
				"Item " . LIBCAL_BARCODE_LABEL => $this->item_barcode,
				"Item Value" => $this->item_value,
				"WARNING" => $this->warning_message
				);
			}
			
			$output = "";
			foreach( $arrayFine as $key => $value){
				if($value){
					$output .= $key . ": " . $value . "; ";
				}
			}
			return $output;
			
		}


	}	 // end class fine

/**************************************************************************************************
 *  Purpose: container class for grouping Fine objects by patron for reporting 
 **************************************************************************************************/
class PatronFineGroup{
	
	private $group_id = "";        // holds patron email address as unique ID for patron(since all LibCal bookings must have email)
	private $patron_details = ""; // string for holding patron name, email, and ID number
	private $items_array = array(); // associative array with item ID as key and and array of Fine objects as value
	
	function __construct($fineObj){
		
		$this->group_id = $fineObj->getPatronEmail();
		$this->patron_details = $fineObj->getPatronDetails();
		$itemId = $fineObj->getItemId();
		$this->items_array[$itemId] = array($fineObj);
	}

	
	function getPatronDetails(){
		return $this->patron_details;
	}
	
	function getFineItems(){
		return $this->items_array;
	}

	/***************************************************************************************************
	 * Purpose: load the given Fine object into the $items_array associative array property; 
	 *          if the item_id exists as an assoc. array key, load the fine into the existing value array;
	 *          if the item_id does not exist as a key, add it, along with a new Fine array containing the object as value.	 
	 * @param Fine object $fineObj 
    **************************************************************************************************/	
	function addItem(&$fineObj){
		$itemId = $fineObj->getItemId();
		if(array_key_exists($itemId, $this->items_array)){
			array_push($this->items_array[$itemId], $fineObj);
		} else {
			$this->items_array[$itemId] = array($fineObj);
		}
	}

	/***************************************************************************************************
	 * Purpose: Return a reporting string containing the details for the patrons fine items, grouped by item.
	 * @return: string 
    **************************************************************************************************/	
	function outputGroup(){
		$tab = "&nbsp;&nbsp;&nbsp;";
		$patronOutput = $this->patron_details . LINEBREAK;
		$itemOutput = "";
		$itemGroup = array();
		foreach($this->items_array as $itemId => $fineArray){
			$newItemFlag = false;
			foreach($fineArray as $fineObj){
				if(! $newItemFlag){
					$itemOutput .= $tab . $fineObj->getItemDetails() . LINEBREAK;
					$newItemFlag = true;
				}
				$itemOutput .= $tab . $tab . $fineObj->getFineDetails() . LINEBREAK;
			}
		}
		return $patronOutput . $itemOutput . LINEBREAK;
	}
			
}	

	
?>