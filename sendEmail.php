<?php

	error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_STRICT);

	set_include_path("." . PATH_SEPARATOR . ($UserDir = dirname($_SERVER['DOCUMENT_ROOT'])) . "/pear/php" . PATH_SEPARATOR . get_include_path());
	require_once "Mail.php";
	
	
	function sendMail($errLog, $from, $to, $subject, $body, $cc = "", $bcc = "", $reply_to="", $sendHTML = true){

		$host = "XXXX.XXXX.ca";
		$port = "XX";
	
		$recipients = $to;
		$reply_to = (empty($reply_to)) ? $from : $reply_to;
		
		$headers = array (
			'From' => $from, 
			'To' => $to, 
			'Subject' => $subject, 
			'Reply-To' => $reply_to 
			);
			
		if($sendHTML){
			$headers['MIME-Version'] = 1;
			$headers['Content-type'] = 'text/html;charset=iso-8859-1';
		}	
		if(!empty($cc)){ 
			$headers['Cc'] = $cc;
			$recipients = $recipients . "," . $cc;
		}
		if(!empty($bcc)){ 
			$headers['Bcc'] = $bcc;
			$recipients = $recipients . "," . $bcc;
		}
		
		$smtp = Mail::factory('smtp', array ('host' => $host, 'port' => $port, 'auth' => false));
		$mail = $smtp->send($recipients, $headers, $body);
		
		$sendDate = date_format(new DateTime(), 'Y-m-d H:i:s');
		$emailLogPrefix = "\n" . $sendDate . ": Subject: " . $headers['Subject'] . " ";

		if (PEAR::isError($mail)) {
			$emailErrMsg = $emailLogPrefix .  "Error sending email: " . $mail->getMessage() . "\n";
			logEmailError($emailErrMsg, $errLog);
			return false;
		} else {
			return true;
		}
				
	}
	
	/********************************************************************************************
	 * Purpose: appends a datestamped string with the given error string to a log file
	 * Given: $error a string or an Exception object;
	 *        $logfile, a filepath and name for the log file to write to
	 *       
	 * Effect: Appends the error message to the log file or throws and exception if it cannot write to file.
	 **********************************************************************************************/ 
	function logEmailError($error, $logFile){		
			$logEntry = date("Y-m-d H:i:s") . "\n" . $error . "\n\n";
		try{
			writeToFile($logEntry, $logFile, "a");
		} catch (Exception $e){
			error_log(print_r( $e->getMessage()));
			// do nothing
		}
	}

?>