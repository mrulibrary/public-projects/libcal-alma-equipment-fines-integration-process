<?php
	/** 
		Utility routines for use with LibCal API scripts		
		@author mgauthier@mtroyal.ca
	*/


	/********************************************************************************************
	 * Purpose: appends a datestamped string with the given error string to a log file
	 * Given: $error a string or an Exception object;
	 *        $logfile, a filepath and name for the log file to write to
	 *        $isException, an optional boolean flag: if true, the $error parameter contains and Exception object,
	 *                      else it contains a string.
	 * Effect: Appends the error message to the log file or throws and exception if it cannot write to file.
	 **********************************************************************************************/ 
	function logError($error, $logFile){		
			$logEntry = date("Y-m-d H:i:s") . "\n" . $error . "\n\n";
		try{
			// change log filename if apache is executing the script 
			$userInfo = posix_getpwuid(posix_getuid());
			$user = $userInfo['name'];    
			if(preg_match('/apache/i', $user)){	
				$logFile = preg_replace('|\.|', '_apache.', $logFile);
			}
			writeToFile($logEntry, $logFile, "a");
		} catch (Exception $e){
			error_log(print_r( $e->getMessage()));
			// do nothing
		}
	}

	/********************************************************************************************
	 * Purpose: Returns an access token
	 * Given: $tokenFile, a string containing path & filename for the stored token file;
	 *        $maxAge, a string containing a valid API client id;
	 *        $credsArray, an array containing the credentials for obtaining the token via the API;
	 *        $errorFile, a string containing the path & file name for the error log file
	 * Returns: an access token string, may be empty.
	 * Effects: Throws and exception if unable to get valid access token
	 **********************************************************************************************/ 	
	function getToken($tokenFile, $maxAge, $credsArray){
		$token = "";
		//first, check for existing token
		if(file_exists($tokenFile) && checkFileAge($tokenFile, $maxAge)){
			// get  stored token
			include ($tokenFile);
			
			if(defined('TOKEN') && TOKEN != ''){ // make sure token in file is not empty (happens if api authorization fails)
				$token = TOKEN;
			}
		} 
		if(empty($token)) { // if no token file, empty token or token older than max age, get new token
			try {
				$token = getNewToken($credsArray["url"] . "oauth/token", $credsArray["client_id"], $credsArray["client_secret"], $credsArray["grant_type"]);
				
				if($token != ""){
					// store token as variable in php file
					writeToAuthFile($token, $tokenFile);
				}
			} catch (Exception $e){
				throw new Exception("Authorization Error: " . $e->getMessage() .  "\n");
			}
		}
		return $token;
	}
	
	/********************************************************************************************
	 * Purpose: Gets an access token via the API
	 * Given: $endpoint, a string containing a valid API url;
	 *        $clientId, a string containing a valid API client id;
	 *        $secret, a string containing the secret value for the API call;
	 *        $grantType, a string containing the API access grant type.
	 * Returns: string containing access token, else empty string.
	 * Effects: Throws and exception if unable to get valid access token
	 **********************************************************************************************/ 
	function getNewToken($endpoint, $clientId, $secret, $grantType){
		$success = true;
		$accessToken = "";
		
		$params = array(
			"client_id" => $clientId,
			"client_secret" => $secret,
			"grant_type" => $grantType);		

		$curl = curl_init($endpoint);
		
		curl_setopt($curl, CURLOPT_HEADER, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_HEADER,'Content-Type: application/x-www-form-urlencoded');

		$postData = "";

		//This is needed to properly post the credentials object
		foreach($params as $k => $v){
		   $postData .= $k . '='.urlencode($v).'&';
		}

		$postData = rtrim($postData, '&');

		curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
		$json_response = curl_exec($curl);	

		// evaluate for success response
		$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);	
		if ($status != 200) {
			throw new Exception("Error: call to URL $endpoint failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl) . "\n");
			$success = false;
		}
		curl_close($curl);	
		
		if($success){
			$authObj = json_decode($json_response);
			if(isset($authObj->error)){
				$errMsg = $authObj->error;
				if(isset($authObj->error_description)){
					$errMsg.= " " . $authObj->error_description;
				}
				throw new Exception("Authorization Error: " . $errMsg .  "\n");
			} else {
				$accessToken = $authObj->access_token;
				$expires_in = $authObj->expires_in;
			}
			
		}
		return $accessToken;
			
	}



	// API call to retrieve availability 
	function getAPIData($url, $authToken){
		
		$authorization = "Authorization: Bearer " . $authToken;
		$crl = curl_init();
		curl_setopt($crl, CURLOPT_URL, $url);
		curl_setopt($crl, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
		curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($crl, CURLOPT_HTTPGET, 1);
		$curl_response = curl_exec($crl);
		$status = curl_getinfo($crl, CURLINFO_HTTP_CODE);	
		
		if ($status != 200) {
			throw new Exception("Error: call to URL $url failed with status $status, response $curl_response, curl_error " . curl_error($crl) . ", curl_errno " . curl_errno($crl) . "\n");
			$success = false;
		}
		curl_close($crl);
		return $curl_response;
		
	}
	 

	 /********************************************************************************************
	 * Purpose: checks the last modified time of the given file relative to the current time
	 * Given: a string containing a valid file path and name,
	 *        an int representing the max modification age for the given file in seconds
	 * Returns: true if last mod time of given file is less than the given max number of seconds;
	 *          else false.
	 **********************************************************************************************/ 
	function checkFileAge($file, $max_seconds){
		 $curTime = time();
		 if(file_exists($file)){
			$fileModTime = filectime($file);
			if($curTime - $fileModTime < $max_seconds){
				return true;
			}
		 }
		 return false;
	 }

	/********************************************************************************************
	 * Purpose: write the given string to the given file, using the given optional mode
	 * Given: $string, a string;
	 *        $file, a filepath and name
	 *        $mode, an optional mode, assumes w for write if none is supplied. See php documentation fopen possible mode values) 
	 * Effect: Writes the given string to the given file. If optional mode is supplied, uses that mode, else assume 'w' for 'write' mode
	 **********************************************************************************************/ 
	function writeToFile($string, $file, $mode = "w"){
		$handle = null;
		
		if(! file_exists($file)	){
			$handle = fopen($file, $mode);
			// set read/write permissions for the file
			chmod($file, 0664);
		} else {
			$handle = fopen($file, $mode);
		}
		
		if($handle){
			fwrite($handle, $string);
			fclose($handle);
			return true;
		} 
		return false;
	}
		
	// store the given string as the TOKEN variable in the file with the given filename
	function writeToAuthFile($string, $file){
		
		$content =  "<?php define('TOKEN'" . ",'" . $string . "'); ?>";
		return writeToFile($content, $file);
	}
		
	// Read and return the contents of the given file
	function readFromFile($file){
		$contents = file_get_contents($file);
		return $contents;	
	}

	/**********************************************************************************
 * Purpose: Given a date string in UTC formats, and a string containing the date format 
 *          required (e.g. "d-m-Y H:i:s"), returns the date in given format;
 * @param string $strDate date in the format: 2017-04-06T07:30:00-06:00
 * @param string $format format of return string
 * @return string containing a time in the given format 
 ***********************************************************************************/ 
function getFormattedDateFromUTC($strDate, $format){
	$time = strtotime($strDate);
	$localDate = date($format, $time);
	return $localDate;
}
	
?>